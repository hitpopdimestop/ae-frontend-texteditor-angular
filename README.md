## Run App

ng serve

## How to use

##### - Select any word and click on any of the buttons to change its style
##### - Double click on any word to load it's synonyms. If synonyms exist select any of them in the dropdown menu to replace
##### - Click anywhere on the text to edit
