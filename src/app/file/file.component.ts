import { Component, HostListener, OnInit } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { WordInterface } from '../models/word.interface';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  text = '';
  synonyms: WordInterface[];
  rangeBuffer: Range;
  style;

  @HostListener('document:click', ['$event'])
  onClick(ev: MouseEvent) {
    this.synonyms = null;
  }

  constructor(private textService: TextService) {
  }

  onDblClick() {
    const word = window.getSelection() ? window.getSelection().toString() : null;
    this.rangeBuffer = window.getSelection().getRangeAt(0).cloneRange();

    if (word) {
      this.textService.getSynonyms(word)
        .subscribe((data: WordInterface[]) => {
          if (data.length) {
            const clientRect = this.textService.getBoundingClientRect(this.rangeBuffer);
            clientRect ? this.style = {'top': `${clientRect.top + clientRect.height}px`, 'left': `${clientRect.left}px`} :
              this.style = null;
            this.synonyms = data;
          } else {
            // we may show that synonyms not found
          }
        });
    }
  }

  changeWord(word: string): void {
    this.textService.changeWord(this.rangeBuffer, word);
  }

  ngOnInit(): void {
    this.textService.getMockText().then((result) => {
      this.text = result;
    });
  }

}
