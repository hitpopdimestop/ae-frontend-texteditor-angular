export interface WordInterface {
  score: number;
  word: string;
  tags: string[];
}
