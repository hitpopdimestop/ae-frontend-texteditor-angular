import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { WordInterface } from '../models/word.interface';



@Injectable()
export class TextService {

  constructor(private http: HttpClient) {}

  getMockText() {
    return new Promise<string>(function (resolve) {
      resolve('A year ago I was in the audience at a gathering of designers in San Francisco. ' +
        'There were four designers on stage, and two of them worked for me. I was there to support them. ' +
        'The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. ' +
        'What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, ' +
        'that modern design problems were very complex. And we ought to need a license to solve them.');
    });
  }

  changeWord(range: Range, word: string) {
    range.deleteContents();
    range.insertNode(document.createTextNode(word));
  }

  getSelection(): Selection {
    if (window.getSelection) {
      const selection = window.getSelection();
      if (selection.rangeCount === 1 && !selection.isCollapsed) {
        return selection;
      }
    }
    return null;
  }

  getBoundingClientRect(range?: Range): ClientRect {
    if (range) {
      return range.getBoundingClientRect();
    }
    const selection = this.getSelection();
    if (!selection) {
      return null;
    }

    range = selection.getRangeAt(0);
    return range ? range.getBoundingClientRect() : null;
  }

  applyTagToSelection(tag: string): void {
    const selection = this.getSelection();
    if (!selection) {
      return;
    }

    const range = selection.getRangeAt(0);
    if (range.startContainer.childNodes.length > 1 && range.startContainer.localName !== tag) {
      range.selectNodeContents(range.startContainer.childNodes[range.startOffset]);
    }

    const nodeWithSameTagInRange = this.findNodeWithSameTag(range.startContainer, tag);
    if (nodeWithSameTagInRange) {
      // removing tag, but keeping content
      nodeWithSameTagInRange['outerHTML'] = nodeWithSameTagInRange['innerHTML'];
    } else {
      // surrounding contents with tag if tag node with same localName isn't found
      const element = document.createElement(tag);
      range.surroundContents(element);
    }
  }

  findNodeWithSameTag(node: Node, tag: string): Node {
    if (node.nodeName === tag.toUpperCase()) {
      return node;
    } else if (node.nodeName === 'DIV') {
      return null;
    } else {
      return this.findNodeWithSameTag(node.parentNode, tag);
    }
  }

  getSynonyms(word: string) {
    const url = `https://api.datamuse.com/words?ml=${word}`;
    return this.http.get(url)
      .pipe(
        map((data: Array<WordInterface>) => data.filter(wrd => wrd && wrd.tags && wrd.tags.some(tag => tag === 'syn')))
      );
  }
}
